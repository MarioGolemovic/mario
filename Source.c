#define _CRT_SECURE_NO_WARNINGS
#include "StdLibs.h"
#include "Grupe.h"
#include "Utils.h"

int main() {

	const char* fileName = "grupe.txt";

	printf("\t\tDobrodosli na malonogometnu super ligu.\n\n");

	GRUPA* grupa = alocirajGrupu();

	unosIzDatoteke(grupa, fileName);

	TIM* pobjednik = odigrajTurnir(grupa);

	printf("Pobjednik lige je ");

	fputs(pobjednik->imeTima, stdout);

	grupa = oslobodiGrupu(grupa);

	return 0;
}